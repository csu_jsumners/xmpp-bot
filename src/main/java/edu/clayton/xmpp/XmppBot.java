package edu.clayton.xmpp;

import java.util.List;

public interface XmppBot {
  /**
   * <p>Used to start the bot.</p>
   *
   * @return true if the bot successfully connected to the server.
   */
  public boolean connect();

  /**
   * <p>Determines if the bot will attempt to connect to the server with
   * a username and password or anonymously. Implementations should default
   * to connecting anonymously.</p>
   *
   * <p>If this used to set the property to false, then the
   * {@link XmppBot#setCredentials(String, String)} method must be invoked.</p>
   *
   * @param value True (default) to connect anonymously, false otherwise.
   */
  public void shouldConnectAnonymously(boolean value);

  /**
   * @return True if the bot will connect anonymously, false otherwise.
   */
  public boolean connectAnonymously();

  /**
   * <p>XMPP bots rely on a {@link org.jivesoftware.smack.ConnectionListener}
   * in order to monitor disconnections/reconnections.
   * This method <strong>must</strong> be invoked after successfully
   * connecting to the XMPP server.</p>
   *
   * @param listener An implementation of {@link XmppBotConnectionListener}.
   */
  public void setConnectionListener(XmppBotConnectionListener listener);

  /**
   * <p>Used to disconnect the bot from the server.</p>
   */
  public void disconnect();

  /**
   * <p>This method will be invoked after the bot has successfully connected
   * to the chat server. It will attempt to join the channels specified with
   * {@link XmppBot#setChannels(java.util.List)}.</p>
   */
  public void joinChannels();

  /**
   * <p>A list of XMPP channel names the bot should be able to join with
   * {@link edu.clayton.xmpp.XmppBot#joinChannels()}. Each channel name
   * should be fully qualified. For example, to join the channel "foo" on the
   * "example.com" server the name should be "foo@conference.example.com"
   * (or very similar).</p>
   *
   * @param channels The channels to join.
   */
  public void setChannels(List<String> channels);

  /**
   * <p>Sets the username and password to use when not connecting anonymously.</p>
   *
   * @param username The XMPP username.
   * @param password The XMPP password.
   */
  public void setCredentials(String username, String password);
}