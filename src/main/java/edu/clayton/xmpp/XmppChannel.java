package edu.clayton.xmpp;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Represents a XMPP multi-user channel. A bot will use at least
 * one instance of this class.</p>
 */
public abstract class XmppChannel extends XmppPacketListener {
  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  protected String channelName;
  protected Connection connection;
  protected MultiUserChat channel;
  protected String nickName;

  public XmppChannel(Connection connection, String name, String nickName) {
    this.connection = connection;
    this.channelName = name;
    this.nickName = nickName;
  }

  /**
   * <p>Attempts to join the channel.</p>
   */
  public void join() {
    this.channel = new MultiUserChat(this.connection, this.channelName);
    DiscussionHistory discussionHistory = new DiscussionHistory();
    discussionHistory .setMaxStanzas(0);

    try {
      log.info("Joining channel {}", this.channelName);
      this.channel.join(this.nickName, null, discussionHistory, 1000);
      this.channel.addMessageListener(this);
    } catch (XMPPException e) {
      log.error("Could not join channel {}", this.channelName);
      log.debug(e.toString());
    }
  }

  /**
   * <p>Whenever a message is sent to the channel this method will be invoked
   * with an instance of {@link Message}. You should override this method
   * if you would like to do anything useful with it. Otherwise, what would be
   * the point of the bot?</p>
   *
   * @param message The incoming chat message.
   */
  public abstract void parseMessage(Message message);

  /**
   * <p>Send a simple {@link String} message to the channel.</p>
   *
   * @param message The message to send.
   */
  public void sendMessage(String message) {
    try {
      log.debug("Sending message to channel `{}`", this.channelName);
      this.channel.sendMessage(message);
    } catch (XMPPException e) {
      log.error("Could not send message to channel");
      log.debug(e.toString());
    }
  }
}