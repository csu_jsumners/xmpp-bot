package edu.clayton.xmpp;

import org.jivesoftware.smack.ConnectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class XmppBotConnectionListener implements ConnectionListener {
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private XmppBot bot;
  private int maximumReconnectionAttempts;
  private int reconnectionAttempts = 0;

  public XmppBotConnectionListener(XmppBot bot) {
    this(bot, -1);
  }

  /**
   * <p>Creates an instance of {@linkplain XmppBotConnectionListener} and
   * sets the maximum number of times it will attempt to reconnect upon
   * disconnection.</p>
   *
   * @param bot An instance of {@link XmppBot}.
   * @param maxReconnectAttempts The number of times to attempt reconnection.
   */
  public XmppBotConnectionListener(XmppBot bot, int maxReconnectAttempts) {
    this.bot = bot;
    this.maximumReconnectionAttempts = maxReconnectAttempts;
  }

  /**
   * <p>Will be invoked when the server connection is closed due to normal
   * operation.</p>
   */
  public void connectionClosed() {
    this.log.info("Bot connection closed normally");
  }

  /**
   * <p>Will be invoked when the the server connection is interrupted due
   * to an unexpected error.</p>
   *
   * @param e The unexpected error that caused the disconnection.
   */
  public void connectionClosedOnError(Exception e) {
    this.log.error("Bot connection closed abnormally");
    this.log.debug(e.toString());

    this.bot.disconnect();
  }

  /**
   * <p>Will be invoked every second of the interval between a disconnection
   * and the bot attempting to reconnect.</p>
   *
   * @param seconds The number of seconds until a reconnection attempt.
   */
  public void reconnectingIn(int seconds) {
    if (this.reconnectionAttempts <= this.maximumReconnectionAttempts) {
      this.log.debug("Bot reconnecting in {} seconds", seconds);
    } else {
      this.log.debug("Maximum reconnection attempts reached");
      this.bot.disconnect();
    }
  }

  /**
   * <p>Will be invoked when a reconnection is successfully performed.</p>
   */
  public void reconnectionSuccessful() {
    this.log.info("Bot reconnected successfully");

    this.log.info("Rejoining channels");
    this.bot.joinChannels();

    this.reconnectionAttempts = 0;
  }

  /**
   * <p>Will be invoked when a reconnection has failed due to an
   * unexpected error.</p>
   *
   * @param e The error that caused the failed attempt.
   */
  public void reconnectionFailed(Exception e) {
    this.log.error("Bot could not reconnect");
    this.log.debug(e.toString());

    this.bot.disconnect();

    if (this.reconnectionAttempts < this.maximumReconnectionAttempts) {
      this.reconnectionAttempts += 1;

      this.log.info("Attempting reconnect #{}", this.reconnectionAttempts);
      this.bot.connect();
    }
  }
}