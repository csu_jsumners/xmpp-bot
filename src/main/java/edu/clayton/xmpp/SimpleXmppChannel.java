package edu.clayton.xmpp;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.packet.Message;

public class SimpleXmppChannel extends XmppChannel {
  public SimpleXmppChannel(Connection connection, String name, String nickName) {
    super(connection, name, nickName);
  }

  @Override
  public void parseMessage(Message message) {
     this.log.debug("Received message to parse");
  }
}