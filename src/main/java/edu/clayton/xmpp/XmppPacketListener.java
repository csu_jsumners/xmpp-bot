package edu.clayton.xmpp;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class XmppPacketListener implements PacketListener {
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public void processPacket(Packet packet) {
    if (packet == null || packet.getPacketID() == null) {
      log.debug("Got a null packet. Not doing anything with it");
      return;
    }

    log.debug("Received packet with id = `{}`", packet.getPacketID());
    log.debug(packet.toXML());

    Message message = (Message) packet;
    this.parseMessage(message);
  }

  /**
   * <p>When a valid packet is received, this method will be invoked with
   * the packet represented as a {@link Message}. This should be the
   * only method implementing classes are concerned about.</p>
   * @param message
   */
  public abstract void parseMessage(Message message);
}