package edu.clayton.xmpp;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SimpleXmppBot implements XmppBot {
  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  protected List<XmppChannel> channels;
  protected List<String> channelNames;
  protected Connection connection;
  protected boolean connectAnonymously;
  protected XmppBotConnectionListener listener;
  protected String nickName;
  protected String password;
  protected String serverUrl;
  protected String userername;

  public SimpleXmppBot() {}

  public SimpleXmppBot(String serverUrl, String nickName) {
    this(serverUrl, nickName, "", "");
  }

  public SimpleXmppBot(String serverUrl, String nickName, String userername, String password) {
    this.connectAnonymously = true;
    this.serverUrl = serverUrl;
    this.nickName = nickName;
    this.userername = userername;
    this.password = password;
  }

  public boolean connect() {
    boolean didConnect = false;
    this.connection = new XMPPConnection(this.serverUrl);

    try {
      log.info("Connecting to {}", this.serverUrl);
      this.connection.connect();

      if (this.connectAnonymously()) {
        log.info("Logging in anonymously");
        this.connection.loginAnonymously();
      } else {
        log.info("Logging in with specified credentials");
        this.connection.login(this.userername, this.password);
      }

      didConnect = true;
    } catch (XMPPException e) {
      log.error("Could not connect to {}", this.serverUrl);
      log.debug(e.toString());
    }

    return didConnect;
  }

  public void shouldConnectAnonymously(boolean value) {
    this.connectAnonymously = value;
  }

  public boolean connectAnonymously() {
    return this.connectAnonymously;
  }

  public void setConnectionListener(XmppBotConnectionListener listener) {
    this.listener = listener;
  }

  public void disconnect() {
    if (this.connection != null && this.connection.isConnected()) {
      this.connection.disconnect();
    }
  }

  public void joinChannels() {
    if (this.channelNames == null ||
        this.connection == null ||
        this.nickName == null)
    {
      return;
    }

    if (this.channels == null || this.channels.size() == 0) {
      this.createChannels();
    }

    for (XmppChannel channel : this.channels) {
      channel.join();
    }
  }

  public void setChannels(List<String> channelNames) {
    this.channelNames = channelNames;
  }

  public void setCredentials(String username, String password) {
    this.userername = username;
    this.password = password;
  }

  public String getNickName() {
    return this.nickName;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getServerUrl() {
    return this.serverUrl;
  }

  public void setServerUrl(String serverUrl) {
    this.serverUrl = serverUrl;
  }

  public String getUserername() {
    return this.userername;
  }

  public void setUserername(String userername) {
    this.userername = userername;
  }

  private void createChannels() {
    this.channels = new ArrayList<>(0);

    for (String name : this.channelNames) {
      XmppChannel channel =
          new SimpleXmppChannel(this.connection, name, this.nickName);
      this.channels.add(channel);
    }
  }
}