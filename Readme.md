# xmpp-bot #

Xmpp-bot is a library for creating XMPP chat bots. This library provides relatively simple interfaces to the [Smack API](http://www.igniterealtime.org/projects/smack/). These interfaces make it easy to get a bot up and running.

For and example bot, see the `SimpleXmppBot` class. This class can be extended for a quick and easy chat bot. You merely have to override the methods that specific to your situation; very likely, these would be either the `joinChannels` or `setChannels` methods.

## Downloading ##

This library is available in a public [Maven repository](https://bitbucket.org/csu_jsumners/public-maven-repo).
To add the dependency to your project, add the following repository and dependency definitions to your
pom.xml:

    <repository>
	  <!-- CSU Releases Repo -->
	  <id>csu-bitbucket-public</id>
	  <url>https://bitbucket.org/csu_jsumners/public-maven-repo/src/master/releases/</url>
    </repository>
	<repository>
	  <!-- CSU Snapshots Repo -->
	  <id>csu-bitbucket-public-snapshots</id>
	  <url>https://bitbucket.org/csu_jsumners/public-maven-repo/src/master/snapshots/</url>
	</respository>

    <dependency>
      <groupId>edu.clayton</groupId>
      <artifactId>xmpp-bot</artifactId>
      <version>1.2</version>
    </dependency>

Otherwise, feel free to clone this repository and use the code as you wish.

## License ##

The MIT License (MIT)

Copyright (c) 2013 Clayton State University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
